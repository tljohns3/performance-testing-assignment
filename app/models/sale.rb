class Sale < ActiveRecord::Base
  max_paginates_per 20
  belongs_to :customer


  def amount_with_tax
    self.amount + self.tax
  end
end
